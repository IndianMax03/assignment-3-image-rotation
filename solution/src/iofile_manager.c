//
// Created by Raj Manu on 07.01.2023.
//

#include "calculator.h"
#include "image_manager.h"
#include "iofile_manager.h"

enum BMP_HEADER_VALUES {
    BUFFER_TYPE = 0x4D42,
    BUFFER_IS_RESERVED = 0,
    BUFFER_OFFTOP_BITS = sizeof(struct bmp_header),
    BITMAP_INFOHEADER_SIZE = 40,
    BITMAP_PLANES = 1,
    BITMAP_BITCOUNT = 24,
    BITMAP_COMPRESSION = 0,
    BITMAP_X_PELS_PER_METER = 2834,
    BITMAP_Y_PELS_PER_METER = 2834,
    BITMAP_COLOR_USED = 0,
    BITMAP_COLOR_IMPORTANT = 0,
};

const char* const read_error_messages[] = {
        [READ_OK] = "the reading was successful",
        [READ_INVALID_FILE] = "the file turned out to be bad",
        [READ_INVALID_SIGNATURE] = "the signature turned out to be bad",
        [READ_INVALID_BITS] = "the bits turned out to be bad",
        [READ_INVALID_HEADER] = "the header turned out to be bad",
        [READ_INVALID_PIXELS_READING] = "pixels' reading went wrong",
        [READ_INVALID_ALLOCATING] = "cannot allocate memory"
};

const char* const write_error_messages[] = {
        [WRITE_OK] = "the writing was successful",
        [WRITE_ERROR] = "the writing went wrong"
};

const char* reading_get_cause(enum read_status status) {
    return read_error_messages[status];
}

const char* writing_get_cause(enum write_status status) {
    return write_error_messages[status];
}

enum read_status fill_pixels(FILE* in, struct image* const read, const long padding) {
    uint64_t row = 0;
    /* for each reading image row */
    while (row < read->height) {
        /* reading into a pixels' structure */
        size_t read_pix = fread(read->data + row * read->width, sizeof (struct pixel_t) * read->width, 1, in);
        /* and extends by padding */
        size_t seek_pix = fseek(in, padding, SEEK_CUR);
        /* catching an error if got problem with reading or seeking (0 when success) */
        if (read_pix < 1 || seek_pix) {
            return READ_INVALID_PIXELS_READING;
        }
        row++;
    }
    return READ_OK;
}

enum read_status from_bmp( FILE* in, struct image* const read ) {

    if (!in) {
        return READ_INVALID_FILE;
    }

    struct bmp_header header_buffer;

    size_t result = fread(&header_buffer, sizeof(struct bmp_header), 1, in);

    if (result != 1) {
        return READ_INVALID_HEADER;
    }
    if (header_buffer.bfType != BUFFER_TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    if (header_buffer.biBitCount != BITMAP_BITCOUNT) {
        return READ_INVALID_BITS;
    }

    *read = allocate_image(header_buffer.biHeight, header_buffer.biWidth);

    if (!read->data) {
        return READ_INVALID_ALLOCATING;
    }

    const long padding = calculate_padding(read->width);

    return fill_pixels(in, read, padding);

}

struct image rotate( struct image const source, size_t (*rot_ptr)(size_t, size_t, uint64_t)) {

    struct image result = allocate_image(source.width, source.height);

    if (!result.data) {
        return (struct image) {0};
    }

    for (size_t row = 0; row < result.height; row++) {
        for (size_t column = 0; column < result.width; column++) {
            size_t old_index = rot_ptr(source.height - 1 - column, row, source.width);
            size_t new_index = rot_ptr(row, column, result.width);
            result.data[new_index] = source.data[old_index];
        }
    }
    return result;
}

static struct bmp_header create_bmp_header(struct image const* img, long padding) {
    return (struct bmp_header) {
            .bfType = BUFFER_TYPE,
            .bfileSize = (img->width * 3 + padding)  * img->height + sizeof(struct bmp_header),
            .biSizeImage = (img->width * 3 + padding)  * img->height,
            .bfReserved = BUFFER_IS_RESERVED,
            .bOffBits = BUFFER_OFFTOP_BITS,
            .biSize = BITMAP_INFOHEADER_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BITMAP_PLANES,
            .biBitCount = BITMAP_BITCOUNT,
            .biCompression = BITMAP_COMPRESSION,
            .biXPelsPerMeter = BITMAP_X_PELS_PER_METER,
            .biYPelsPerMeter = BITMAP_Y_PELS_PER_METER,
            .biClrUsed = BITMAP_COLOR_USED,
            .biClrImportant = BITMAP_COLOR_IMPORTANT
    };
}

enum write_status to_bmp( FILE* out, struct image const* img ) {

    if (!out || !img) {
        return WRITE_ERROR;
    }

    long padding = calculate_padding(img->width);

    struct bmp_header header_buffer = create_bmp_header(img, padding);

    size_t writing = fwrite(&header_buffer, sizeof (struct bmp_header), 1, out);

    if (writing) {

        for (size_t row = 0; row < img->height; row++) {
            size_t written = fwrite(img->data + row * img->width, sizeof (struct pixel_t), img->width, out);
            size_t seeked = fseek(out, padding, SEEK_CUR);
            if (written < 1 || seeked) {
                return WRITE_ERROR;
            }
        }
        return WRITE_OK;
    }
    return WRITE_ERROR;
}
