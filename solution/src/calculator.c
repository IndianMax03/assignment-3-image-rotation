//
// Created by Raj Manu on 08.01.2023.
//

#include "calculator.h"

long calculate_padding(const uint64_t width_pix) {
    return (width_pix % 4) == 0 ? 0 : (long) (4 - 3 * width_pix % 4);
}
