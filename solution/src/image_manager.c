//
// Created by Raj Manu on 07.01.2023.
//
#include "image_manager.h"
#include <malloc.h>

struct image empty_image(void) {
    return (struct image) {0};
}

struct image allocate_image(const uint64_t height, const uint64_t width) {
    struct image result = {0};
    if (height <= 0 || width <= 0) {
        return result;
    }
    result.data = (struct pixel_t*) malloc(height * width * sizeof (struct pixel_t));
    if (result.data) {
        result.height = height;
        result.width = width;
    }
    return result;


}

void free_images(struct image* original_img, struct image* modified_img) {
    free(original_img->data);
    free(modified_img->data);
}
