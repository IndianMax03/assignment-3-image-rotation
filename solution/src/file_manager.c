//
// Created by Raj Manu on 07.01.2023.
//

#include "file_manager.h"
#include <stdio.h>

struct files_handler get_files(char* argv[]) {

    struct files_handler result = {
            .src_file = fopen(argv[1], "rb"),
            .dest_file = fopen(argv[2], "wb"),
            .is_valid = true
    };

    if (!result.src_file) {
        printf("Can't open the source file for reading.\n");
        result.is_valid = false;
    }
    if (!result.dest_file) {
        printf("Can't open the destination file for writing.\n");
        result.is_valid = false;
    }

    if (!result.is_valid) close_files(result);

    return result;
}

void close_files(struct files_handler files) {
    fclose(files.src_file);
    fclose(files.dest_file);
    files.is_valid = false;
}
