#include "file_manager.h"
#include <stdio.h>

#define OK_CODE 0
#define ERR_CODE 1

size_t rotate_90_left(const size_t row, const size_t column, const uint64_t width) {
    return row * width + column;
}

int main(int argc, char **argv) {

    if (argc != 3) {
        printf("Invalid number of arguments. Try using: image-transformer <src-img> <transformed-img>.\n");
        return 1;
    }

    struct files_handler files = get_files(argv);

    if (!files.is_valid) return ERR_CODE;

    struct image original_img = empty_image();

    enum read_status r_status = from_bmp(files.src_file, &original_img);
    if (r_status != READ_OK) {
        printf("Reading error: %s", reading_get_cause(r_status));
        close_files(files);
        return ERR_CODE;
    }

    struct image modified_img = rotate(original_img, rotate_90_left);

    enum write_status w_status = to_bmp(files.dest_file, &modified_img);
    if (w_status != WRITE_OK) {
        printf("Writing error: %s", writing_get_cause(w_status));
        close_files(files);
        return ERR_CODE;
    }

    free_images(&original_img, &modified_img);

    close_files(files);

    return OK_CODE;
}

