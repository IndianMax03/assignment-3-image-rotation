//
// Created by Raj Manu on 08.01.2023.
//

#ifndef IMAGE_TRANSFORMER_CALCULATOR_H
#define IMAGE_TRANSFORMER_CALCULATOR_H

#include <stdint.h>
#include <stdio.h>

long calculate_padding(uint64_t width_pix);

#endif //IMAGE_TRANSFORMER_CALCULATOR_H
