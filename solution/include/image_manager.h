//
// Created by Raj Manu on 07.01.2023.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_MANAGER_H
#define IMAGE_TRANSFORMER_IMAGE_MANAGER_H

#include <stdint.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;

    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

struct __attribute__((packed)) pixel_t {
    uint8_t b,g,r;
};

struct image {
    uint64_t width, height;
    struct pixel_t* data;
};

struct image empty_image(void);
void free_images(struct image* original_img, struct image* modified_img);
struct image allocate_image(uint64_t height, uint64_t width);

#endif //IMAGE_TRANSFORMER_IMAGE_MANAGER_H
