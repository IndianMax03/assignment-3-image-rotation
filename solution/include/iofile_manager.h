//
// Created by Raj Manu on 07.01.2023.
//

#ifndef LAB_3_IMAGE_ROT_STUB_H
#define LAB_3_IMAGE_ROT_STUB_H

#include "image_manager.h"
#include <stdio.h>

/* deserializer */
enum read_status {
    READ_OK = 0,
    READ_INVALID_FILE,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PIXELS_READING,
    READ_INVALID_ALLOCATING
};

/* serializer */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

const char* reading_get_cause(enum read_status status);
const char* writing_get_cause(enum write_status status);
enum read_status from_bmp( FILE* in, struct image* read );
enum write_status to_bmp( FILE* out, struct image const* img );
struct image rotate(struct image source, size_t (*rot_ptr)(size_t, size_t, uint64_t));

#endif //LAB_3_IMAGE_ROT_STUB_H
