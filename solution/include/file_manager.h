//
// Created by Raj Manu on 07.01.2023.
//

#ifndef IMAGE_TRANSFORMER_FILE_MANAGER_H
#define IMAGE_TRANSFORMER_FILE_MANAGER_H

#include "image_manager.h"
#include "iofile_manager.h"
#include <stdbool.h>

struct files_handler {
    FILE* src_file;
    FILE* dest_file;
    bool is_valid;
};

void close_files(struct files_handler files);

struct files_handler get_files(char* argv[]);

#endif //IMAGE_TRANSFORMER_FILE_MANAGER_H
